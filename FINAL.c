#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <conio.h>

void clase();
void ingresoClase(int op);
void empezarJuego(char palabras[][15], char nombre []);
void dibujo (int intentos);

int main(){
    clase();
	system("pause");
    return 0;
}

void clase(){
int op;
do{
	system("cls");
    printf("\n EL AHORCADO \n\n");
    printf(" CLASE\n\n");
    printf(" 1. CALCULO\n");
    printf(" 2. PROGRAMACION\n");
    printf(" 3. QUIMICA\n");
    printf(" 4. ALGEBRA\n\n");
	printf(" Ingresa una opcion: ");
	scanf("%i", &op);

}while(op<1 || op>4 );

	if (op==1) ingresoClase(op);
	if (op==2) ingresoClase(op);
	if (op==3) ingresoClase(op);
	if (op==4) ingresoClase(op);
}

void ingresoClase (int op){
	char nombreclas[4][15] = {"CALULO","PROGRAMACION","QUIMICA","ALGEBRA"};
	char CALCULO [10][15] = {"diferencial","integral","funcion","derivada","factorizar","exponente","monomio","polinomio","raiz","fraciones"};
	char PROGRAMACION [10][15] = {"lenjuage","sistema","compilador","consola","ejecutar","caracteres","cadenas","variables","flotante","gitlab"};
	char QUIMICA [10][15] = {"covalente","enlace","redes","polar","estructura","constante","oxidacion","electroquimic","balanceo","reduccion"};
	char ALGEBRA [10][15] = {"exponencial","suma","formulas","operaciones","matriz","determinantes","invers","elementales","cramer","vectores"};

switch(op){
		case 1:
			empezarJuego(CALCULO,nombreclas[op-1]);
			break;
		case 2:
			empezarJuego(PROGRAMACION,nombreclas[op-1]);
			break;
		case 3:
			empezarJuego(QUIMICA,nombreclas[op-1]);
			break;
		case 4:
			empezarJuego(ALGEBRA,nombreclas[op-1]);
			break;
	}
}

void empezarJuego (char palabras[][15], char nombre[]){
	int opcion,i,j,k,longitud,espacios;
	char letra;
	int aciertos = 0;
	int intentos = 0;
	int ganar = 0;

opcion = rand() % 10;
 longitud = strlen(palabras[opcion]);
 char frase[longitud];


 for(i=0; i < longitud; i++){
		frase[i] = '_';
	}

do{
		aciertos = 0;
		system("cls");
		printf("\nJUEGO EL AHORCADO\n\n");
		printf(" CLASE: %s\n\n",nombre);
		printf(" Intentos Disponibles: %i",6-intentos);
dibujo(intentos);
printf("\n\n\n");
		for(i=0; i < longitud; i++){
			printf(" %c ",frase[i]);
		}
if (intentos == 6){
			printf("\n\n GAME OVER\n");
			printf(" LA SOLUCION ERA: %s\n\n",palabras[opcion]);
			printf(" Presiona una tecla para volver a jugar..");
			getch();
			clase();
		}
        espacios=0;

		for (i = 0; i < longitud; i++){
			if (frase[i] == '_')
				espacios++;
		}
		if (espacios == 0){
			printf("\n\n YOU WINNER\n\n");
			printf(" Presiona una tecla para volver a jugar..");
			getch();
			clase();
		}

		printf("\n\n Digite una letra: ");
		scanf(" %c",&letra);


			for (j = 0; j < longitud; j++){
			if (letra == palabras[opcion][j]){
				frase[j] = letra;
				aciertos++;
			}
		}
if (aciertos == 0){
			intentos++;
}

}while(intentos != 7);
printf("\n\n");
}
void dibujo (int intentos){
	switch(intentos){
		case 0:
			printf("\n     _______\n    |       |\n    |\n    |\n    |\n    |\n    |\n ----------");
			break;
		case 1:
			printf("\n     _______\n    |       |\n    |       0\n    |\n    |\n    |\n    |\n ----------");
			break;
		case 2:
			printf("\n     _______\n    |       |\n    |       0\n    |       |\n    |\n    |\n    |\n ----------");
			break;
		case 3:
			printf("\n     _______\n    |       |\n    |       0\n    |      /|\n    |\n    |\n    |\n ----------");
			break;
		case 4:
			printf("\n     _______\n    |       |\n    |       0\n    |      /|");
			printf("\\");
			printf("\n");
			printf("    |\n    |\n    |\n ----------");
			break;
		case 5:
			printf("\n     _______\n    |       |\n    |       0\n    |      /|");
			printf("\\");
			printf("\n");
			printf("    |      /\n    |\n    |\n ----------");
			break;
		case 6:
			printf("\n     _______\n    |       |\n    |       0\n    |      /|");
			printf("\\");
			printf("\n");
			printf("    |      / ");
			printf("\\");
			printf("\n");
			printf("    |\n    |\n ----------");
			break;
	}

}


